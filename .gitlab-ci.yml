# Jobs within a given stage are run in parallel. Stages are run in sequence (note 3 jobs in the 'Render' stage).
stages:
    - "Test"
    - "Parse"
    - "Render"
    - "Gather"

image: "registry.gitlab.com/coedl/mini-wlp"

"Run test suite":
  stage: "Test"
  before_script:
  # For our production repository, we install the latest version of coedl/yinarlingi to run tests. But for this mini-wlp demo pipeline,
  # we've pre-installed a specific version of yinarlingi appropriate for the demo pipeline, so the install...() line below is commented out.
    # - Rscript -e 'devtools::install_github("coedl/yinarlingi", dependencies = c("Depends", "Imports", "Suggests"))'

  # Copy the test-suite RMarkdown file from the installed package into the tmp directory
    - Rscript -e 'file.copy(from = system.file("rmarkdown/test-suite.Rmd", package = "yinarlingi"), to = "tmp/", overwrite = TRUE)'
  script:
  # Run the test suite with rmarkdown::render(). Note that 'lexicon_path' parameter is relative to where the Rmarkdown file is located (i.e. inside the tmp folder)
    - Rscript -e 'rmarkdown::render(input = "tmp/test-suite.Rmd", params = list(lexicon_path = "../src/wlp-lexicon_master.txt"))'

  # Check that the rendered HTML file contains no elements with the class 'failing-test-block'. If there are any, fail the pipeline by causing a non-zero exit code (exit 1)
    - if grep --quiet 'class="failing-test-block"' tmp/test-suite.html; then mo templates/gitlab-mo/failed-test-suite.mo && exit 1; fi
  artifacts:
    when: always
    paths:
      - tmp

"Parse into JSON":
  stage: "Parse"
  before_script:
  # Generate the skeleton version of the Warlpiri lexicon, filtering out undesired codes
    - Rscript -e 'writeLines(yinarlingi::skeletonise_df("src/wlp-lexicon_master.txt")$data, "tmp/wlp-skeleton.txt")'

  # Compile the Nearley grammar into Javascript code
    - nearleyc scripts/parser/wlp-skeleton.ne > tmp/wlp-skeleton.js

  # Symbolically link globally installed node.js packages for use within the current working directory
    - npm link nearley moo nomnom
  script:
  # Split the tmp/wlp-skeleton.txt file by main entry chunks and parse each chunk with the wlp-skeleton.js grammar, and collect all chunks into a single JSON object 'wlp_lex'
    - node scripts/parser/split-n-parse.js --grammar tmp/wlp-skeleton.js --data tmp/wlp-skeleton.txt --split '(?=\\me)' --name wlp_lex > tmp/wlp-skeleton.json

  # If there were any parse errors in the resulting JSON file, fail the pipeline.
    - if grep --quiet 'Error\|Incomplete parse' tmp/wlp-skeleton.json; then echo "Parsing wlp-skeleton.txt failed. Check tmp/wlp-skeleton.json for errors." && exit 1; fi
  artifacts:
    when: always
    paths:
      - tmp

"to web app":
  stage: "Render"
  dependencies:
    - "Parse into JSON"
  before_script:
  # Symbolically link globally installed node.js packages for use within the current working directory
    - npm link ejs-cli
  script:
  # As the skeleton template references other templates (e.g. cross-refs.ejs), run ejs-cli from within the directory containing the relevant template files
    - cd templates/skeleton-www && ejs-cli _wlp-skeleton.ejs -O ../../tmp/wlp-skeleton.json > ../../tmp/wlp-lexicon.html && cd ../../

  # If any ejs-cli error output occured, fail the job
    - if grep --quiet /usr/lib/node_modules/ejs-cli/ tmp/wlp-lexicon.html; then echo "Rendering wlp-lexicon.html failed; check file for errors." &&  exit 1; fi
  artifacts:
    when: always
    paths:
      - tmp

"to InDesign XML":
  stage: "Render"
  dependencies:
    - "Parse into JSON"
  before_script:
  # Symbolically link globally installed node.js packages for use within the current working directory
    - npm link ejs-cli
  script:
  # We split the tmp/wlp-skeleton.json file into different sections (e.g. 'j', 'ng', etc.) so the designer can import 1 chapter at a time
    - node templates/skeleton-xml/split-into-sections.js
  # Find all wlp-section-*.json within the tmp folder, then for each section, apply the ejs template, and write its output to a .xml file of the same name
    - cd templates/skeleton-xml && find ../../tmp/ -name 'wlp-section-*.json' -exec sh -c 'ejs-cli _wlp-skeleton.ejs -O {} > {}.xml' ';' && cd ../../

  # Check for xml well-formedness using xmllint and output the results to xmllint-results.csv
    - find tmp -name '*.xml' | parallel --results tmp/xmllint-results.csv xmllint --noout {} ';'

  # If any error returned from xmllint, fail the job
    - if grep --quiet 'parser error' tmp/xmllint-results.csv; then echo "Rendering to InDesign XML failed; check xmllint-results.csv for errors." &&  exit 1; fi
  artifacts:
    when: always
    paths:
      - tmp

"to PDF (via xelatex)":
  stage: "Render"
  dependencies:
    - "Parse into JSON"
  before_script:
  # Symbolically link globally installed node.js packages for use within the current working directory
    - npm link ejs-cli
  script:
  # As the skeleton template references other templates (e.g. cross-refs.ejs), run ejs-cli from within the directory containing the relevant template files
    - cd templates/skeleton-tex && ejs-cli _wlp-skeleton.ejs -O ../../tmp/wlp-skeleton.json > ../../tmp/wlp-lexicon.tex && cd ../../

  # Run xelatex on the generated wlp-lexicon.tex file to get the PDF
    - cd tmp && xelatex -interaction=nonstopmode wlp-lexicon.tex && cd ../
  artifacts:
    when: always
    paths:
      - tmp

# This job must be called 'pages' in order for the GitLab pipeline to know to deploy the public folder to https://coedl.gitlab.io/mini-wlp
pages:
  stage: "Gather"
  dependencies:
    - "to web app"
    - "to InDesign XML"
    - "to PDF (via xelatex)"
  before_script:
    - mkdir public
  script:
  # Copy over the PDF and single-page web app
    - cp tmp/wlp-lexicon.{pdf,html} public/
  # Zip up all the xml files
    - find tmp -name *.xml -print | zip public/wlp-xml.zip -@
  # Create HTML page, listing download links to rendered data (PDF, etc.), and pipeline information
    - DATE=$(date) mo templates/gitlab-mo/deploy-page.mo > public/index.html
  artifacts:
    when: always
    paths:
      - public
