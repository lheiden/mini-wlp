# Grammar for parsing Warlpiri dictionary entries  into a JSON file
# using Moo (lexer/tokeniser generator: https://github.com/no-context/moo)
# and Nearley (parser generator: https://nearley.js.org/)
# 
# Notes:
# 1. Production rules are nested so that you can collapse blocks of child elements when in a text editor like Sublime Text
#
# 2. Moo returns objects such as { "type": "LineData", "value": "-ja*2* (V-SFX):" } (see: https://github.com/no-context/moo#token-info)
#
# 3. Post-processor Javascript functions are defined in the {% ... %} blocks following the production rules
#    We use array destructuring assignment to refer to the elements in the data array passed into the post-processor function
#    so that we don't have to re-write array indicies data[0], data[1], etc. when the right hand side of a production rules changes
#    (see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#Array_destructuring)
#
#                                                                                    Raw text ... Moo  ...    Nearley  ...   JSON output
# 4. Case of variable names are picked to be as transparent as possible                     AABB -> AaBb -> aaBb -> _aa_bb -> aa_bb
#    - UpperCamelCase for token objects returned from Moo                                   (e.g. NewLine, LineData, etc.)
#    - lowerCamelCase for non-terminal symbols within the production rules                  (e.g. mainEntry -> ... entryBody senseEntry:* subEntry:*)
#    - _underscore-initial lower-case names for named post-processor functions              (e.g. _extract_value, _split_value)
#    - lower_case with underscores for JSON object key names in returned by post-processors (e.g. { "entry_label": ... })
#

@{%
// Lexer
const MOO = require('moo')

const WLPLEXER = MOO.compile({
    NewLine: {
        match: /\n+/,
        lineBreaks: true
    },

    BSCode: {
        match: /\\[a-z|A-Z]+\s?/,
        value: x => x.replace(/^\\/, '').replace(/\s$/, '') // Remove initial backslash codes for convenience "\me" => "me"
    },

    LineData: {
        match: /[^\\]+/, lineBreaks: true,
        value: x => x.replace(/\s[\n\t]*$/, '')            // Remove trailing whitespace/newlines/tabs
    },

    SourceInfo: {
        match: /\s?\\\[[^\]]+\]\s?/,
        lineBreaks: true
    }
})

// Helper post-processor functions for Moo tokens in 2nd position
const _extract_value = ([startCode, {value}, SourceInfo, endCode]) => value               // "\gl car \egl"        => "car"
const _split_value   = ([startCode, {value}, SourceInfo, endCode]) => value.split(/,\s?/) // "\ant car, hat \eant" => ["car", "hat"]
const _null_if_empty = (arr) => arr.length == 0 ? null : arr                              // [] => null

const _format_sense = ([se_Sub, LineData, SourceInfo, entryBody, ese_Sub]) => Object.assign(
    // Merge entryBody object {} with another object
    // { "sense_label": ... } to add sense_label key
    entryBody,                                                    
    { "sense_label": LineData != null ? LineData.value : null }
)

%}

@lexer WLPLEXER

mainEntry -> "me" %LineData %SourceInfo:* entryBody "eme" senseEntry:* subEntry:*
    {%
        function([me, LineData, SourceInfo, entryBody, eme, senseEntry, subEntry]) {
            
            [digraph, letter1, letter2] = LineData.value.match(/^-?([A-Z|a-z])([a-z])?/);
            
            return {
                "entry_label":   LineData.value,
                "entry_section": ["ng", "ny", "rd"].indexOf(digraph.toLowerCase()) != -1 ? digraph : letter1,
                "sense_entries": [entryBody].concat(senseEntry),                  // [1,2,3].concat([4]) => [1,2,3,4]
                "sub_entries":   _null_if_empty(subEntry)
            }
        }
    %}

    subEntry -> "sse" %LineData %SourceInfo:* entryBody "esse" senseEntry:*
        {%
            function([sse, LineData, SourceInfo, entryBody, esse, senseEntry]) {
                return {
                    "entry_label":   LineData.value,
                    "sense_entries": [entryBody].concat(senseEntry),
                }
            }
        %}

    senseEntry -> "se"  %LineData:? %SourceInfo:* entryBody "ese"  {% _format_sense %}
              |   "sub" %LineData:? %SourceInfo:* entryBody "esub" {% _format_sense %}

    paradigmExample -> ("pdx" | "pdxs") %LineData %SourceInfo:* entryBody ("epdx" | "epdxs")
        {%
            function([pdxPdxs, LineData, SourceInfo, entryBody, epdxEpdxs]) {
                return Object.assign(
                    entryBody,
                    { "pdx_label": LineData.value }
                )
            }
        %}

    entryBody -> englishOrigin:? semanticDomain:* definition:? latinName:? englishGloss:? reversal:? comment:* (exampleBlock:+ | paradigmExample:+):? crossRefs
        {%
            function([englishOrigin, semanticDomain, definition, latinName, englishGloss, reversal, comment, egOrPdxNode, crossRefs]) {
                let return_obj = {
                    "eng_origin":   englishOrigin,
                    "sem_domain":   _null_if_empty(semanticDomain),
                    "definition":   definition,
                    "lat_name":     latinName,
                    "comments":     _null_if_empty(comment),
                    "glosses":      englishGloss,
                    "cross_refs":   crossRefs
                }

                if(egOrPdxNode == null) {
                    return return_obj
                } else {
                    const [egOrPdxArray]   = egOrPdxNode
                    const egOrPdxKey       = egOrPdxArray.every(o => o.pdx_label) ? "pdg_examples" : "examples"

                    return_obj[egOrPdxKey] = egOrPdxArray

                    return return_obj
                }
            }
        %}

        semanticDomain -> "dm" %LineData %SourceInfo:* "edm"
            {% _extract_value %}

        englishOrigin -> "org" %LineData %SourceInfo:* "eorg"
            {% _extract_value %}

        definition -> "def" %LineData %SourceInfo:* "edef"
            {% _extract_value %} 

        latinName -> "lat" %LineData %SourceInfo:* "elat"
            {% _extract_value %} 

        englishGloss -> "gl" %LineData %SourceInfo:* "egl"
            {% _extract_value %}

        reversal -> "rv" %LineData %SourceInfo:* "erv"
            {% _extract_value %}

        comment -> "cm"   %LineData %SourceInfo:* "ecm"
            {% _extract_value %}

        exampleBlock -> "eg" comment:* examplePair:+ "eeg"
            {% ([eg, comment, examplePairs, eeg]) => ({ "comments": _null_if_empty(comment), "example_pairs": examplePairs }) %}

            examplePair -> egWlpLine egEngLine ("ewe" | "ewed"):?
                {% ([egWlpLine, egEngLine, eweEwed]) => ({ "wlp": egWlpLine, "eng": egEngLine }) %}

                egWlpLine -> ("we" | "wed") %LineData %SourceInfo:*
                    {% ([weWed, LineData, SourceInfo]) => LineData.value %}

                egEngLine -> "et" %LineData %SourceInfo:*
                    {% _extract_value %}

        crossRefs -> antonym:? compareWlp:? compareSL:? preverbList:? synonym:?
            {%
                function(crossRefs) {
                    if(crossRefs.every(xref => xref == null)) {
                        return null
                    } else {
                        const [antonym, compareWlp, compareSL, preverbList, synonym] = crossRefs
                        return {
                            "antonyms":  antonym,
                            "comp_wlps": compareWlp,
                            "comp_sl": compareSL,
                            "preverbs": preverbList,
                            "synonyms":  synonym
                        }
                    }
                }
            %}

            antonym -> "ant"  %LineData %SourceInfo:* "eant"
                {% _split_value %}

            compareWlp -> "cf"   %LineData %SourceInfo:* "ecf"
                {% _split_value %}

            compareSL -> "csl"   %LineData %SourceInfo:* "ecsl"
                {% _split_value %}

            preverbList -> "pvl"   %LineData %SourceInfo:* "epvl"
                {% _split_value %}

            synonym -> "syn" %LineData %SourceInfo:* "esyn"
                {% _split_value %}
